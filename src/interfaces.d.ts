// App interfaces
interface MyAppProps {
}

interface MyAppState {
}

// CloneApp interfaces
interface MyCloneProps {
    users: any;
}

interface MyCloneState {
    users: object;
}

// Dropdown interfaces
interface MyDropdownProps {
    isOpenedProp: boolean;
    defaultName: string;
}

interface MyDropdownState {
    isOpened: boolean;
    name: string;
}