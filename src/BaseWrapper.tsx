import * as React from 'react';
import styled from 'styled-components';
import { Rotate, StyledLogo } from './styled';

const StyledAppDiv = styled.div`
text-align: center;
`;

const StyledAppHeaderDiv = styled.div`
  background-color: #222;
  height: 150px;
  padding: 20px;
  color: white;
  font-family: Geneva, Arial, Helvetica, sans-serif
`;

const logo = require('./logo.svg');

interface MyProps {
}

interface MyState {
}

class BaseWrapper extends React.Component<MyProps, MyState> {
    render() {
        return (
            <div>
                <StyledAppDiv>
                    <StyledAppHeaderDiv className="someClassName">
                        <Rotate><StyledLogo src={logo} alt="logo"/></Rotate>
                        <h2>Welcome to React epta</h2>
                    </StyledAppHeaderDiv>
                </StyledAppDiv>
            </div>

        );
    }
}

export default BaseWrapper;
