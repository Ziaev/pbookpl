import * as React from 'react';
import styled from 'styled-components';

const StyledAppDiv = styled.div`
text-align: center;
`;

const StyledAppParagraph = styled.p`
  font-size: large;
`;

class CloneApp extends React.Component<MyCloneProps, MyCloneState> {
    constructor(props: MyCloneProps) {
        super(props);
    }

    render() {
        const {users} = this.props.users;

        return (
            <StyledAppDiv>
                <StyledAppParagraph>
                    === ReactJS, Typescript, Webpack homework CloneApp ===
                    {console.log(users)}
                </StyledAppParagraph>
                <div>
                    <ul>
                        {users}
                    </ul>
                </div>
            </StyledAppDiv>
        );
    }
}

export default CloneApp;
