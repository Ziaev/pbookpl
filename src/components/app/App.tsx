import * as React from 'react';
import Dropdown from '../../plugins/dropdown';
import { StyledAppDiv, StyledAppParagraph, Title, Wrapper } from '../../styled';

class App extends React.Component<MyAppProps, MyAppState> {
    render() {
        return (
            <div>
                <StyledAppDiv>
                    <StyledAppParagraph>
                        ReactJS, Typescript, Webpack homework App.
                    </StyledAppParagraph>
                    <Dropdown defaultName="KPA6" isOpenedProp={false}/>
                </StyledAppDiv>

                <Wrapper>
                    <Title>
                        This is my first styled component!
                    </Title>
                </Wrapper>
            </div>
        );
    }
}

export default App;
