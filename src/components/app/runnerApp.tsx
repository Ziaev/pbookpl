import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import styled from 'styled-components';

const StyledApp = styled(App)`
body {
  margin: 0;
  padding: 0;
  font-family: Geneva, Arial, Helvetica, sans-serif;
}

@keyframes App-logo-spin {
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
}
`;

ReactDOM.render(
    <StyledApp/>,
    document.getElementById('container') as HTMLElement
);