import * as React from 'react';
import * as ReactDOM from 'react-dom';
import BaseWrapper from './BaseWrapper';
import App from './components/app/App';

ReactDOM.render(
    <BaseWrapper />,
    document.getElementById('basewrapper') as HTMLElement
);

ReactDOM.render(
    <App/>,
    document.getElementById('appcontainer') as HTMLElement
);