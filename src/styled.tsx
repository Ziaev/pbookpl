import styled, { keyframes } from 'styled-components';

export const StyledAppDiv = styled.div`
text-align: center;
`;
export const StyledAppParagraph = styled.p`
  font-size: large;
  font-family: Roboto Regular;
`;
export const Title = styled.h1`
    font: Roboto Regular;
	font-size: medium;
	text-align: center;
	color: palevioletred;
`;
export const Wrapper = styled.section`
	padding: 4em;
	background: papayawhip;
`;

export const Input = styled.input`
	padding: 0.5em;
	margin: 0.5em;
	color: palevioletred;
	background: papayawhip;
	border: none;
	border-radius: 3px;
`;

export const rotate360 = keyframes`
	from {
		transform: rotate(0deg);
	}

	to {
		transform: rotate(360deg);
	}
`;

export const Rotate = styled.div`
	display: inline-block;
	animation: ${rotate360} 2s linear infinite;
	font-size: 1.2rem;
`;

export const StyledLogo = styled.img`
    height: 80px;
`;