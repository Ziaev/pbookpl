import { injectGlobal } from 'styled-components';

injectGlobal`
    * { margin: 500px; padding: 0; };
    @font-face {
    font-family: 'Roboto Regular';
    src: url('../fonts/Roboto-Regular.ttf');
  }
`;