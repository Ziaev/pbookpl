## ReactJS, Webpack, Typescript test app
Учебное приложение

### Описание
Приложение имеет 2 встроенных бандла: App.bundle.js, CloneApp.bundle.js. Переключение между приложениями происходит вызовом метода openApp('bundlename.js'). Изначально загружается бандл App.bundle.js.

### TODO:
1. Подключить редукс
2. Попробовать сделать редьюсер с кликом на странице как в примере редукс видео №6
3. Подключить БД
4. ~~Добавить systemjs в externals SystemJS: System~~
5. stylelint rc stylelint processor styled-components
6. ~~настроить pipeline~~
7. stylelint в webpack
8. соединить с беком 
9. appcontainer, shared c basewrapper (header+footer)
10. git flow изучить
11. Подключить redux, redux-react
12. изучить js замыкание
13. Сделать counter app
14. Сделать TODO App
15. Сделать phonesApp
16. Соеднить его с беком
17. Сделать Spring App c xml конфигами и бинами, БД и hibernate
18. deploy to heroku/aws
19. deploy to 'linux' container
20. Заюзать текстовую базу данных (найти ее сначала)
21. Почитать про селекторы