const path = require('path');
const webpackCopy = require('copy-webpack-plugin');

module.exports = {
    entry: {
        App: './src/index.tsx',
        MainApp: './src/components/app/runnerApp.tsx',
        CloneApp: './src/components/cloneapp/runnerCloneApp.tsx',
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'source-map',

    resolve: {
        extensions: ['.webpack.js', '.web.js', '.css', '.ts', '.js', '.json', '.tsx']
    },
    module: {
        rules: [
            {test: /\.tsx?$/, loader: 'awesome-typescript-loader'},
            {test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/, use: "file-loader"}
        ]
    },
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
        'systemjs': 'SystemJS'
    },
    plugins: [
        new webpackCopy([
            {
                from: 'index.html'
            },
            {
                from: 'node_modules/react/dist/react.js', to: 'extlib/react'
            },
            {
                from: 'node_modules/react-dom/dist/react-dom.js', to: 'extlib/reactDOM'
            },
            {
                from: 'node_modules/systemjs/dist/system.js', to: 'extlib/systemjs'
            },
        ])
    ]
};